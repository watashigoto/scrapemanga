//v1

HtmlNodeCollection nodesImages = null;

public String getURLTest() {
	return "https://onepiecechapters.com/chapters/85/one-piece-chapter-995";
}

public Tuple<String, int, List<String>> getChapterInfo(HttpClient httpClient, String inputURL)
{
	String title = null;
	int ctrImage = 0;
	List<String> listPages = new List<String>();
	listPages.Add(inputURL);
	
	HtmlDocument htmlDoc = AppTool.getHTMLDocument(inputURL);
	
	if (htmlDoc != null)
	{
		HtmlNode node = htmlDoc.DocumentNode.SelectSingleNode("//h1[contains(concat(' ',normalize-space(@class),' '),' mt-8 ')]");

		if (node != null)
		{
			title = node.InnerText.Trim();
			title = title.Replace(" - Chapter " , " ");
		}
		
		nodesImages = htmlDoc.DocumentNode.SelectNodes("//img[contains(concat(' ',normalize-space(@class),' '),' fixed-ratio-content ')]");

		if (nodesImages != null)
		ctrImage = nodesImages.Count;
		
		node = null;
		htmlDoc = null;
	}
	
	return Tuple.Create(title, ctrImage, listPages);
}

public void download(HttpClient httpClient, String downloadPath, List<String> listPages, int digitCount, IDownload iDownload)
{
	if (nodesImages != null)
	{
		int totalImage = nodesImages.Count;
		
		for (int i=0; i < totalImage; i++)
		{
			String URL = nodesImages[i].GetAttributeValue("src", null);
			bool isOK = AppTool.downloadImageBool(httpClient, downloadPath, URL, i+1, digitCount);
			iDownload.update(isOK, i+1, URL);
		}
		
		nodesImages.Clear();
		nodesImages = null;
	}
}
