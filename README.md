# ScrapeManga
### Plugin Based Manga Downloader
[Download Here](https://gitlab.com/watashigoto/scrapemanga/-/releases)

Ever frustrated when you want to download a manga but the site changed layout and now you need to wait for the devs to update the application to make it work again?

This application use [Xpath](https://devhints.io/xpath) to parse the page.
By only updating the plugins, you don't need to wait for the devs to update the application just to fix a website, just fix it yourself!

## Planned Features

Download using Authentication


## Installation

Just extract the zip and run the exe.
If you are below Windows 10, you need to install at least [.NET Framework 4.6](https://dotnet.microsoft.com/download/dotnet-framework).

## Supported Sites

[List](https://gitlab.com/watashigoto/scrapemanga/-/tree/main/sites)

## Screenshots
![Screenshot 1](/uploads/b0e9393ad58ec708481b313fe8798b3c/sc1.JPG)

![Screenshot 2](/uploads/2a380abcaf44cf2ded044f7649477be3/sc2.JPG)

![Screenshot 3](/uploads/0ae9e503f12046c7e8c73ae91fb06532/sc3.JPG)
